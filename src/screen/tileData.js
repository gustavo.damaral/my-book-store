import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Favorite, Language, Toys } from '@material-ui/icons';
// import {withRouter} from 'react-router-dom';

const bookCateg = (
    <div>
        <ListItem>
            <ListItemText primary="Categorias" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <Language />
            </ListItemIcon>
            <ListItemText primary="Ficção" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <Toys />
            </ListItemIcon>
            <ListItemText primary="Fantasia" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <Favorite />
            </ListItemIcon>
            <ListItemText primary="Romance" />
        </ListItem>
    </div>
);

export default bookCateg;