import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';
// Divisor para ao drawer
import Divider from '@material-ui/core/Divider';
import { Menu as MenuIcon, ShoppingCart, LocalLibrary } from '@material-ui/icons';
import bookCateg from './tileData';
import { Button, Badge } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

const drawerWidth = 200;

const styles = theme => ({
  root: {
    flexGrow: 1,
    // height: 430,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    position: 'absolute',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  appToolBar: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    // alignItems: 'center',
    justifyContent: 'space-between',
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  pageName: {
    color: 'white',
    fontFamily: 'impact',
    [theme.breakpoints.down(200)]: {
      display: 'none',
    },
  },
  logoName: {
    color: '#3f51b5',
    fontFamily: 'impact',
    [theme.breakpoints.down(400)]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    },
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
  cart: {
    fontSize: 30,
    color: 'white',
  },
  mc1: {
    display: 'flex',
    alignItems: 'center'
  },
  logo: {
    fontSize: 40,
    color: '#3f51b5'
  },
  fullLogo: {
    display: 'flex',
    justifyContent: 'center',
  },
  badge: {
    fontSize: 14,
    width: '70%',
    height: '70%',
    top: -5,
    right: -10,
    // The border color match the background color.
    border: `2px solid #3f51b5`,
  },
});

class ResponsiveDrawer extends React.Component {

  // constructor(props){
  //   super(props);

  // }

  state = {
    mobileOpen: false,
  };

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  render() {
    const { classes, theme, children } = this.props;

    const drawer = (
      <div>
        <div className={classes.toolbar}>
          <div className={classes.fullLogo}>
            <Button onClick={() => this.props.history.push('')}>
              <LocalLibrary className={classes.logo} />
              <Typography variant="title" color="inherit" noWrap className={classes.logoName}>
                BookStore
              </Typography>
            </Button>
          </div>
        </div>
        <Divider />
        <List>{bookCateg}</List>
      </div>
    );

    return (
      <div className={classes.root}>
        <AppBar className={classes.appBar}>
          <Toolbar className={classes.appToolBar}>
            <div className={classes.mc1}>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerToggle}
                className={classes.navIconHide}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="title" color="inherit" noWrap className={classes.pageName}>
                {this.props.pageTitle}
              </Typography>
            </div>
            <IconButton
              onClick={() => this.props.history.push('carrinho')}>
              {this.props.carrinho.length!==0?(<Badge
                badgeContent={this.props.carrinho.length}
                color="secondary"
                classes={{ badge: classes.badge }}>
                <ShoppingCart className={classes.cart} />
              </Badge>):<ShoppingCart className={classes.cart} />}
            </IconButton>
          </Toolbar>
        </AppBar>
        <Hidden mdUp>
          <Drawer
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={this.state.mobileOpen}
            onClose={this.handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {/* <Typography noWrap>{'You think water moves fast? You should see ice.'}</Typography> */}
          {children}
        </main>
      </div>
    );
  }
}

ResponsiveDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withRouter(ResponsiveDrawer));