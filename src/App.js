import React, { Component } from "react";
import "./App.css";
import ResponsiveDrawer from "./screen/ResponsiveDrawer";
import GridLoja from "./components/GridLoja";
import Cart from "./components/Cart";
import { Route, Switch } from "react-router-dom";

class App extends Component {

  constructor() {
    super();

    this.state = {
      valor: 10,
      pageTitle: '',
      carrinho: [],
    }

  }

  updateCart = async (val) => {
    this.setState({
      carrinho: val
    });
  }


  render() {

    const NoMatch = ({ location }) => (
      <ResponsiveDrawer
        containerStyle={{ height: 'calc(100%)' }}
        pageTitle={'Loja'}
      >
        <h1>Página não encontrada!</h1>
      </ResponsiveDrawer>
    );

    return (
      <Switch>
        <Route exact path="/">
          <ResponsiveDrawer
            containerStyle={{ height: 'calc(100%)' }}
            pageTitle={'Loja'}
            carrinho={this.state.carrinho}
          >
            <GridLoja
              valor={this.state.valor}
              carrinho={this.state.carrinho}
              updateCarrinho={this.updateCart}
            />
          </ResponsiveDrawer>
        </Route>
        <Route exact path="/carrinho">
          <ResponsiveDrawer
            containerStyle={{ height: 'calc(100%)' }}
            pageTitle={'Carrinho'}
            carrinho={this.state.carrinho}
          >
            <Cart
              updateCarrinho={this.updateCart}
              carrinho={this.state.carrinho}
            />
          </ResponsiveDrawer>
        </Route>
        <Route component={NoMatch} />
      </Switch>
    );
  }
}

export default App;