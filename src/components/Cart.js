import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import Typography from '@material-ui/core/Typography';

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: '#566ef3',
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        width: '100%',
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: '#f9f9f9',
        },
    },
    bookImg: {
        width: '100%',
        maxWidth: 100,
    },
    qtCell: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#8d9cea',
        '&:hover': {
            backgroundColor: '#566ef3',
        },
    },
});

// let id = 0;
// function createData(name, calories, fat, carbs, protein) {
//   id += 1;
//   return { id, name, calories, fat, carbs, protein };
// }

// const data = [
//   createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//   createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//   createData('Eclair', 262, 16.0, 24, 6.0),
//   createData('Cupcake', 305, 3.7, 67, 4.3),
//   createData('Gingerbread', 356, 16.0, 49, 3.9),
// ];

class CustomizedTable extends React.Component {

    handleQt = (b, tipo) => {

        var index;
        var lista = this.props.carrinho

        if (lista.find(livro => livro.id === b.id)) {
            lista.map(livro => {
                if (livro.id === b.id && livro.qt === 1 && tipo === 'remove') {
                    index = lista.indexOf(lista.find(livro => livro.id === b.id));
                    console.log(index);
                    lista.splice(index, 1);
                }
            });
        }

        if (lista.find(livro => livro.id === b.id)) {
            lista = lista.map(livro => {
                if (livro.id === b.id) {

                    if (tipo === 'add') {
                        ++livro.qt;
                    } else {

                        --livro.qt;

                    }
                }
                return livro;
            });
        }

        this.props.updateCarrinho(lista);

    }

    render() {
        const { classes } = this.props;
        return (
            this.props.carrinho.length === 0 ?
                <Typography variant="title">
                    Carrinho Vazio
                </Typography>
                :
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell>Produto</CustomTableCell>
                                <CustomTableCell numeric><div className={classes.qtCell}>Quantidade</div></CustomTableCell>
                                <CustomTableCell numeric>Valor (R$)</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.carrinho.map(n => {
                                return (
                                    <TableRow className={classes.row} key={n.id}>
                                        <CustomTableCell component="th" scope="row">
                                            <img src={n.img} alt="livro" className={classes.bookImg} />
                                        </CustomTableCell>
                                        <CustomTableCell numeric >
                                            <div className={classes.qtCell}>
                                                <Button
                                                    variant="fab"
                                                    mini
                                                    color="secondary"
                                                    className={classes.button} onClick={() => this.handleQt(n, 'add')}>
                                                    <AddIcon />
                                                </Button>
                                                <Typography variant="subheading">
                                                    {n.qt}
                                                </Typography>
                                                <Button
                                                    variant="fab"
                                                    mini
                                                    color="secondary"
                                                    className={classes.button}
                                                    onClick={() => this.handleQt(n, 'remove')}>
                                                    <RemoveIcon />
                                                </Button>
                                            </div>
                                        </CustomTableCell>
                                        <CustomTableCell numeric>
                                            <Typography variant="subheading">
                                                {(n.price * n.qt).toFixed(2)}
                                            </Typography>
                                        </CustomTableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>
        );
    }
}

CustomizedTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomizedTable);