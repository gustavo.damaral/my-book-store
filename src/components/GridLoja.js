import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Button, ButtonBase } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withRouter } from 'react-router-dom';

import Livros from './Livros';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 200,
    width: 150,
  },
  demo: {
    justifyContent: 'flex-start',
    alignItems: 'left',

  },
  control: {
    padding: theme.spacing.unit * 2,
  },
  bookImg: {
    width: '60%',
    height: '100%',
  },
  bookImgGrid: {
    width: '100%',
    height: '100%',
  },
  dialog: {
    display: 'flex',
  },
  contentDialog: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  titleDialog: {
    fontSize: 100,
  },
  btnBase: {
    '&:hover': {
      zIndex: 1,
      '& $imageBackdrop': {
        opacity: 0.2,
      },
    },
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.white,
    opacity: 0,
    transition: theme.transitions.create('opacity'),
  },
  bookDesc: {
    display: 'flex',
    justifyContent: 'left',
    flexDirection: 'column',
    alignItems: 'flex-end',
    fontSize: 18,
    [theme.breakpoints.down(500)]: {
      fontSize: 12,
    },
  },
});

class GridLoja extends React.Component {
  state = {

  };

  constructor(props) {
    super(props);

    this.state = {
      spacing: '24',
      open: false,
      bookSelected: [],
    }
  }

  handleChange = key => (event, value) => {
    this.setState({
      [key]: value,
    });
  };

  handleClickOpen = (value) => () => {
    this.setState({ bookSelected: value, open: true });
  };
  
  handleClose = () => {
    this.setState({ open: false });
  };
  
  handleBuy = async (b) => {

    console.log(this.props.carrinho);
    var lista = this.props.carrinho
    if (lista.find(livro => livro.id === b.id)) {
      lista = lista.map(livro => {
        if (livro.id === b.id) {
          ++livro.qt;
          return { ...livro };
        }
        return livro;
      });
    } else {
      lista.push(b);
    }
    this.props.updateCarrinho(lista);
    this.handleClose();

  }

  render() {
    const { classes } = this.props;
    const { spacing, bookSelected } = this.state;

    return (
      <Grid container className={classes.root} spacing={16}>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          scroll={'paper'}
          aria-labelledby="scroll-dialog-title"
          className={classes.dialog}
        >
          <DialogTitle className={classes.titleDialog}>{bookSelected.title}</DialogTitle>
          <DialogContent className={classes.contentDialog}>
            <DialogContentText>
              <img src={bookSelected.img} alt="livro" className={classes.bookImg} />
            </DialogContentText>
            <div className={classes.bookDesc}>
              <div>
                {('R$ ' + bookSelected.price)}
              </div>
              <div>
                {bookSelected.author}
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Voltar
                      </Button>
            <Button onClick={() => this.handleBuy(bookSelected)} color="primary">
              Comprar
                      </Button>
          </DialogActions>
        </Dialog>
        <Grid item xs={12}>
          <Grid container className={classes.demo} justify="center" spacing={Number(spacing)}>
            {Livros.map((value, index, array) => (
              <Grid key={value.id} item>
                <ButtonBase onClick={this.handleClickOpen(value)} className={classes.btnBase}>
                  <span className={classes.imageBackdrop} />
                  <Paper className={classes.paper}>
                    <img src={value.img} alt="livrox" className={classes.bookImgGrid} />
                  </Paper>
                </ButtonBase>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

GridLoja.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(GridLoja));